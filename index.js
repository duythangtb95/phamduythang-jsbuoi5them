/* BÀI TẬP 1: TÍNH THUẾ THU NHẬP CÁ NHÂN
 Viết chương trình nhập vào thông tin của 1 cá nhân (Họ tên, tổng thu nhập năm, số
người phụ thuộc). Tính và xuất thuế thu nhập cá nhân phải trả */

function Tinh1() {
    var hoTen = document.getElementById('hoTen').value;
    var tongThunhap = Number(document.getElementById('tongThunhap').value);
    var soNguoiphuthuoc = Number(document.getElementById('soNguoiphuthuoc').value);

    function thue(tongThunhap) {
        if (tongThunhap < 4000000) {
            alert("Số tiền không hợp lệ")
        } else if (tongThunhap <= 60000000) {
            return 0.05;
        } else if (tongThunhap <= 120000000) {
            return 0.1;
        } else if (tongThunhap <= 210000000) {
            return 0.15;
        } else if (tongThunhap <= 384000000) {
            return 0.2;
        } else if (tongThunhap <= 624000000) {
            return 0.25;
        } else if (tongThunhap <= 960000000) {
            return 0.30;
        } else {
            return 0.35;
        }
    }

    var tienThue = (tongThunhap - 4000000 - 1600000 * soNguoiphuthuoc) * thue(tongThunhap);

    document.getElementById('ketQua1').innerText = hoTen;
    document.getElementById('tienThue').innerText = tienThue + "VNĐ";
}

/* BÀI TẬP 2: TÍNH TIỀN CÁP
❖ Viết chương trình tính hóa đơn khách hàng cho một công ty cáp. Có 2 loại khách hàng:
Nhà dân và doanh nghiệp. Có 2 mức giá tính tiền cáp:
1. Nhà dân:
• Phí xử lý hóa đơn: 4.5$
• Phí dịch vụ cơ bản: 20.5$
• Thuê kênh cao cấp: 7.5$ / kênh
2. Doanh nghiệp
• Phí xử lý hóa đơn: 15$
• Phí dịch vụ cơ bản: 75$ cho tổng 10 kết nối đầu, mỗi kết nối thêm 5$ / kết nối
• Thuê kênh cao cấp: 50$ / kênh
❖ Chương trình cho phép nhập vào Mã khách hàng, loại khách hàng, số kết nối, số kênh
cao cấp. Nếu chọn loại khách hàng là Doanh nghiệp ô nhập số kết nối sẽ hiện lên, nếu
chọn loại khách hàng là nhà dân thì ô nhập kết nối sẽ ẩn đi hoặc disabled */

const nhadan = "nhadan";
const doanhnghiep = "doanhnghiep";

// document.getElementById('doanhnghiep').onclick = function {
//     alert(123); 
//     // document.getElementById("soKetnoi").style.display = "block";
// }

function Tinh2() {


    var loaiKhachHang = document.getElementById('loaiKhachHang').value;
    var maKhachHang = document.getElementById('maKhachHang').value;
    var soKenhcaocap = Number(document.getElementById('soKenhcaocap').value);
    var soKetnoi = Number(document.getElementById('soKetnoi').value);

    if (loaiKhachHang == doanhnghiep) {
        if (soKetnoi <= 10) {
            tienCap = 15 + 75 + soKenhcaocap * 50;
        }
        else {
            tienCap = 15 + 75 + soKenhcaocap * 50 + (soKetnoi - 10) * 5;
        }
    } else {
        tienCap = 4.5 + 20.5 + 7.5 * soKenhcaocap;
    }

    document.getElementById('ketQua2').innerText = maKhachHang;
    document.getElementById('tienCap').innerText = tienCap + "$";
}